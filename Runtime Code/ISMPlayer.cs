﻿using com.fdt.genericdatasaving;

namespace com.fdt.statemachinegraph
{
    public interface ISMPlayer: IDataSave
    {
        void Play();
        void SetNode(SMNodeBase n);
        SMNodeBase currentNode { get; set; }
        float nodeInitTime { get; set; }
    }
}