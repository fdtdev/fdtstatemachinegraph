﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using XNode;

namespace com.fdt.statemachinegraph
{
    [CreateAssetMenu (menuName = "FDT/SMGraph/SMGraph", fileName = "SMGraph")]
    public class SMGraph : NodeGraph
    {
        [SerializeField] protected string[] _defaultNodeNamespaces;
        [SerializeField] protected string[] _additionalNodeNamespaces;

        public string[] defaultNodeNamespaces
        {
            get { return _defaultNodeNamespaces; }
        }
        public string[] additionalNodeNamespaces
        {
            get { return _additionalNodeNamespaces; }
        }
        public virtual void CreateCachedNamespaces()
        {
            _defaultNodeNamespaces = new string[2] {"com.fdt.statemachinegraph", "com.fdt.statemachinegraph.common"};
            if (_additionalNodeNamespaces == null)
                _additionalNodeNamespaces = new string[0];
        }
        #if UNITY_EDITOR
        [NonSerialized] public bool changed = false;
        [NonSerialized] public string[] conditionOptions = new string[0];
        [NonSerialized] public string[] settingOptions = new string[0];
        [NonSerialized] public List<ConditionsAsset> conditionObjects = new List<ConditionsAsset>();
        [NonSerialized] public List<StateSettingAsset> settingsObjects = new List<StateSettingAsset>();
        public void ResetOptions()
        {
            List<string> conditions = new List<string>();
            List<string> settings = new List<string>();
            conditionObjects.Clear();
            settingsObjects.Clear();
            conditions.Add("None");
            settings.Add("None");

            var all = AssetDatabase.FindAssets("t:SMContentAsset");
            if (_defaultNodeNamespaces == null || _additionalNodeNamespaces == null)
            {
                CreateCachedNamespaces();
            }
            for (int i = 0; i < all.Length; i++)
            {
                SMContentAsset o = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(AssetDatabase.GUIDToAssetPath(all[i])) as SMContentAsset;
                if (o != null)
                {
                    var tdata = o.GetType().Namespace;
                    if (tdata == null || _defaultNodeNamespaces.Contains(tdata) || _additionalNodeNamespaces.Contains(tdata))
                    {
                        string p = string.Empty;
                        if (!string.IsNullOrEmpty(o.AdditionalFolderData))
                        {
                            p = o.AdditionalFolderData + o.name.ToString();
                        }
                        else
                        {
                            p = o.name.ToString();
                        }

                        if (o is StateSettingAsset)
                        {
                            settings.Add(o.name);
                            settingsObjects.Add(o as StateSettingAsset);
                        }
                        else
                        {
                            conditions.Add(o.name);
                            conditionObjects.Add(o as ConditionsAsset);
                        }
                    }
                }
            }

            conditionOptions = conditions.ToArray();
            settingOptions = settings.ToArray();
            changed = false;
        }
        #endif
    }
}