﻿using com.FDT.Common;
using UnityEngine;

namespace com.fdt.statemachinegraph
{
    [System.Serializable]
    public class TimeRangeData
    {
        public enum TimedType
        {
            NONE = 0,
            RANGE = 1,
            ABSOLUTE = 2,
            RELATIVE = 3
        }

        public TimedType tType;

        [MinMaxRange(0f, 1f)] public Vector2 timeRange;

        public float seconds;
        public bool extrapolate;

        [Range(0, 1f)] public float normalizedTime;

        public bool IsLower(float time, float nTime)
        {
            switch (tType)
            {
                case TimedType.NONE:
                    return false;
                case TimedType.RANGE:
                    return nTime < timeRange.x;
                case TimedType.ABSOLUTE:
                    return time < seconds;
                case TimedType.RELATIVE:
                    return nTime < normalizedTime;
            }
            return false;
        }

        public bool IsHigher(float time, float nTime)
        {
            switch (tType)
            {
                case TimedType.NONE:
                    return false;
                case TimedType.RANGE:
                    bool topped = timeRange.y == 1;
                    if (topped && nTime >= 1 && extrapolate)
                        return false;
                    else
                        return nTime > timeRange.y;
                case TimedType.ABSOLUTE:
                    return time > seconds;
                case TimedType.RELATIVE:
                    return nTime > normalizedTime;
            }
            return false;
        }

        public bool IsHigherTanRange(float time, float nTime)
        {
            if (tType == TimedType.NONE)
                return false;
            else
            {
                return IsHigher(time, nTime);
            }
        }
        public bool IsInRange(float time, float nTime)
        {
            if (tType == TimedType.NONE)
                return true;
            else
            {
                return !IsHigher(time, nTime) && !IsLower(time, nTime);
            }
        }
    }
}