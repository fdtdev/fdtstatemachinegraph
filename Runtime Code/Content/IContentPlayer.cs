﻿namespace com.fdt.statemachinegraph
{
    public interface IContentPlayer: ISMPlayer
    {
        bool[] settingsData { get; }
        bool[] settingsDataAny { get; }
        bool[] conditionsData { get; }
        bool[] conditionsDataAny { get; }
    }
}