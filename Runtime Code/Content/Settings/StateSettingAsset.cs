﻿using com.fdt.genericdatasaving;

namespace com.fdt.statemachinegraph
{
    public abstract class StateSettingAsset : SMContentAsset
    {
        public void ExecuteUpdateAfterFixedUpdate(int contentIdx, SMNodeBase n, ISMPlayer cdata, ValueData[] _valueDatas, 
            float time, float nTime)
        {
            OnExecuteUpdateAfterFixedUpdate(contentIdx, n, cdata, _valueDatas, time, nTime);
        }
        public void ExecuteUpdateBeforeFixedUpdate(int contentIdx, SMNodeBase n, ISMPlayer cdata, ValueData[] _valueDatas, 
            float time, float nTime)
        {
            OnExecuteUpdateBeforeFixedUpdate(contentIdx, n, cdata, _valueDatas, time, nTime);
        }
        
        public void ExecuteUpdateAfterFixedUpdateOutside(int contentIdx, SMNodeBase n, ISMPlayer cdata, ValueData[] _valueDatas,
            float time, float nTime)
        {
            OnExecuteUpdateAfterFixedUpdateOutside(contentIdx, n, cdata, _valueDatas, time, nTime);
        }
        public void ExecuteUpdateBeforeFixedUpdateOutside(int contentIdx, SMNodeBase n, ISMPlayer cdata, ValueData[] _valueDatas,
            float time, float nTime)
        {
            OnExecuteUpdateBeforeFixedUpdateOutside(contentIdx, n, cdata, _valueDatas, time, nTime);
        }
        protected virtual void OnExecuteUpdateAfterFixedUpdate(int contentIdx, SMNodeBase n, ISMPlayer cdata,
            ValueData[] _valueDatas, float time, float nTime)
        {
            
        }
        protected virtual void OnExecuteUpdateBeforeFixedUpdate(int contentIdx, SMNodeBase n, ISMPlayer cdata,
            ValueData[] _valueDatas, float time, float nTime)
        {
            
        }
        protected virtual void OnExecuteUpdateAfterFixedUpdateOutside(int contentIdx, SMNodeBase n, ISMPlayer cdata,
            ValueData[] _valueDatas, float time, float nTime)
        {
            
        }
        protected virtual void OnExecuteUpdateBeforeFixedUpdateOutside(int contentIdx, SMNodeBase n, ISMPlayer cdata,
            ValueData[] _valueDatas, float time, float nTime)
        {
            
        }
        public override bool HasCustomIcon => true;
        public override string IconFileName => "StateSettingAsset Icon";
    }
}