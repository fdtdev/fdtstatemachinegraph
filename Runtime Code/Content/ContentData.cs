﻿using com.FDT.Common;
using com.fdt.genericdatasaving;
using UnityEngine;

namespace com.fdt.statemachinegraph
{
    [System.Serializable]
    public class ContentData
    {
        public bool enabled = true;
        public string description;
        public ValueData[] values;
        [SerializeField] protected TimeRangeData _timeRangeData;

        public TimeRangeData TimeRangeData
        {
            get { return _timeRangeData; }
        }
    }
}