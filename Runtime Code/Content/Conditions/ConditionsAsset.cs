﻿using com.fdt.genericdatasaving;

namespace com.fdt.statemachinegraph
{
    public abstract class ConditionsAsset : SMContentAsset
    {
        // usar este valor para ver en action node si el tipo de setting necesita un valor serializado en el nodo.
        public virtual bool HasValue
        {
            get { return false; }
        }

        public bool CheckCondition(int contentIdx, SMNodeBase n, ISMPlayer cdata, ValueData[] _valueDatas,
            TimeRangeData timeRange, float time, float nTime)
        {
            return OnCheckCondition(contentIdx, n, cdata, _valueDatas, timeRange, time, nTime);
        }
        public virtual bool OnCheckCondition(int contentIdx, SMNodeBase n, ISMPlayer cdata, ValueData[] _valueDatas,
            TimeRangeData timeRange, float time, float nTime)
        {
            return false;
        }

        public virtual bool CheckConditionOutside(int contentIdx, SMNodeBase n, ISMPlayer cdata, ValueData[] _valueDatas,
            TimeRangeData timeRange, float time, float nTime)
        {
            return false;
        }
        
        public override bool HasCustomIcon => true;
        public override string IconFileName => "ConditionsAsset Icon";
    }
}