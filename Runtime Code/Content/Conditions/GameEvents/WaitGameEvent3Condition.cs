﻿
using System;
using System.Collections.Generic;
using com.FDT.GameEvents;
using com.fdt.genericdatasaving;

namespace com.fdt.statemachinegraph
{
    public abstract class WaitGameEvent3Condition<TEvt, TEvtH, T0, T1, T2> : ConditionsAsset where TEvt:GameEvent3<T0, T1, T2> where TEvtH:GameEvent3Handle<T0, T1, T2, TEvt>, new()
    {
#if UNITY_EDITOR
        public override void ResetData()
        {
            base.ResetData();
            listeners.Clear();
        }
#endif

        public class Param1 : IContentParameter
        {
            public VarType GetVarType
            {
                get { return VarType.ASSET; }
            }
            public Type GetAssetType
            {
                get { return typeof(TEvt); }
            }
            public bool GetTypeFromAsset { get; }
            public string GetCustomDescription { get; }
        }
        private IContentParameter[] _params = { new Param1() };
        public override IContentParameter[] Parameters
        {
            get { return _params; }
        }

        public override bool OnCheckCondition(int contentIdx, SMNodeBase n, ISMPlayer cdata, ValueData[] _valueDatas, TimeRangeData timeRange, float time,
            float nTime)
        {
            TEvt evt = _valueDatas[0].AssetValue as TEvt;
            for (int i = 0; i < listeners.Count; i++)
            {
                if (listeners[i].conditionIdx == contentIdx && listeners[i].player == cdata && listeners[i].node == n && listeners[i].evt == evt)
                {
                    return listeners[i].received;
                }
            }

            return false;
        }

        protected abstract bool EventReceived(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, ValueData[] cdValues, TEvt evt, T0 arg1, T1 arg2, T2 arg3);

        public override void EnterNode(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, ValueData[] cdValues, TimeRangeData cdTimeRange, float time,
            float nTime)
        {
            base.EnterNode(contentIdx, smNode, cdata, cdValues, cdTimeRange, time, nTime);
            TEvt evt = cdValues[0].AssetValue as TEvt;
            CreateListener(contentIdx, cdata, smNode, evt, (arg1, arg2, arg3) => 
            {
                for (int i = 0; i < listeners.Count; i++)
                {
                    if (listeners[i].conditionIdx == contentIdx && listeners[i].player == cdata && listeners[i].node == smNode && listeners[i].evt == evt)
                    {
                        listeners[i].received = EventReceived(contentIdx, smNode, cdata, cdValues, listeners[i].evt, arg1, arg2, arg3);
                        if (listeners[i].received)
                        {
                            listeners[i].arg1 = arg1;
                            listeners[i].arg2 = arg2;
                            listeners[i].arg3 = arg3;
                        }
                    }
                }
            });
        }
        public override void ExitNode(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, ValueData[] cdValues, TimeRangeData cdTimeRange, float time,
            float nTime)
        {
            base.ExitNode(contentIdx, smNode, cdata, cdValues, cdTimeRange, time, nTime);
            TEvt evt = cdValues[0].AssetValue as TEvt;
            DestroyListener(contentIdx, smNode, cdata, evt);
        }

        protected List<ListenerData> listeners = new List<ListenerData>();
        public void CreateListener(int contentIdx, ISMPlayer cdata, SMNodeBase node, TEvt evt, Action<T0, T1, T2> callback)
        {
            TEvtH d = new TEvtH();
            d.Event = evt;
            d.AddEventListener(callback);
            
            ListenerData ldata = new ListenerData();
            ldata.evt = evt;
            ldata.handle = d;
            ldata.player = cdata;
            ldata.node = node;
            ldata.conditionIdx = contentIdx;
            listeners.Add(ldata);
        }

        public void DestroyListener(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, TEvt evt)
        {
            for (int i =  listeners.Count-1; i >=0; i--)
            {
                if (listeners[i].conditionIdx == contentIdx && listeners[i].player == cdata && listeners[i].node == smNode && listeners[i].evt == evt)
                {
                    listeners[i].Dispose();
                    listeners.RemoveAt(i);
                }
            }
        }

        [System.Serializable]
        public class ListenerData:IDisposable
        {
            public int conditionIdx;
            public TEvtH handle;
            public ISMPlayer player;
            public TEvt evt;
            public bool received = false;
            public T0 arg1;
            public T1 arg2;
            public T2 arg3;
            public SMNodeBase node;

            public void Dispose()
            {
                handle.Event = null;
                handle = null;
                player = null;
                received = false;
            }
        }
    }
}

