﻿using System;
using System.Collections.Generic;
using com.FDT.GameEvents;
using com.fdt.genericdatasaving;
using UnityEngine;

namespace com.fdt.statemachinegraph
{
    [CreateAssetMenu (menuName = "FDT/SMGraph/WaitGameEventCondition", fileName = "WaitGameEventCondition")]
    public class WaitGameEventCondition : ConditionsAsset
    {
#if UNITY_EDITOR
        public override void ResetData()
        {
            base.ResetData();
            listeners.Clear();
        }
#endif

        class Param1 : IContentParameter
        {
            public VarType GetVarType
            {
                get { return VarType.ASSET; }
            }

            public Type GetAssetType
            {
                get { return typeof(GameEvent); }
            }

            public bool GetTypeFromAsset { get; }
            public string GetCustomDescription { get; }
        }

        protected IContentParameter[] _params = new IContentParameter[] {new Param1()};

        public override IContentParameter[] Parameters
        {
            get { return _params; }
        }

        public override bool OnCheckCondition(int contentIdx, SMNodeBase n, ISMPlayer cdata, ValueData[] _valueDatas,
            TimeRangeData timeRange, float time,
            float nTime)
        {
            GameEvent evt = _valueDatas[0].AssetValue as GameEvent;
            for (int i = 0; i < listeners.Count; i++)
            {
                if (listeners[i].conditionIdx == contentIdx && listeners[i].player == cdata && listeners[i].node == n && listeners[i].evt == evt)
                {
                    
                    return listeners[i].received;
                }
            }

            return false;
        }

        public override void EnterNode(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, ValueData[] cdValues,
            TimeRangeData cdTimeRange, float time,
            float nTime)
        {
            base.EnterNode(contentIdx, smNode, cdata, cdValues, cdTimeRange, time, nTime);
            GameEvent evt = cdValues[0].AssetValue as GameEvent;
            CreateListener(contentIdx, cdata, smNode, evt, () =>
            {
                for (int i = 0; i < listeners.Count; i++)
                {
                    if (listeners[i].conditionIdx == contentIdx && listeners[i].player == cdata && listeners[i].node == smNode && listeners[i].evt == evt)
                    {
                        listeners[i].received = EventReceived(contentIdx, smNode, cdata, cdValues, listeners[i].evt);;
                    }
                }
            });
        }

        public override void ExitNode(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, ValueData[] cdValues,
            TimeRangeData cdTimeRange, float time,
            float nTime)
        {
            base.ExitNode(contentIdx, smNode, cdata, cdValues, cdTimeRange, time, nTime);
            GameEvent evt = cdValues[0].AssetValue as GameEvent;
            DestroyListener(contentIdx, smNode, cdata, evt);
        }

        protected virtual bool EventReceived(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, ValueData[] cdValues, GameEvent evt)
        {
            return true;
        }

        protected List<ListenerData> listeners = new List<ListenerData>();

        public void CreateListener(int contentIdx, ISMPlayer cdata, SMNodeBase node, GameEvent evt, Action callback)
        {
            GameEventHandle d = new GameEventHandle();
            d.Event = evt;
            d.AddEventListener(callback);

            ListenerData ldata = new ListenerData();
            ldata.evt = evt;
            ldata.handle = d;
            ldata.player = cdata;
            ldata.node = node;
            ldata.conditionIdx = contentIdx;
            listeners.Add(ldata);
        }

        public void DestroyListener(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, GameEvent evt)
        {
            for (int i = listeners.Count - 1; i >= 0; i--)
            {
                if (listeners[i].conditionIdx == contentIdx && listeners[i].player == cdata && listeners[i].node == smNode && listeners[i].evt == evt)
                {
                    listeners[i].Dispose();
                    listeners.RemoveAt(i);
                }
            }
        }

        [System.Serializable]
        public class ListenerData : IDisposable
        {
            public int conditionIdx;
            public GameEventHandle handle;
            public ISMPlayer player;
            public GameEvent evt;
            public bool received = false;
            public SMNodeBase node;

            public void Dispose()
            {
                handle.Event = null;
                handle = null;
                player = null;
                received = false;
            }
        }
    }
}