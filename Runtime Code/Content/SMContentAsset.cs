﻿using System;
using System.Collections.Generic;
using com.FDT.Common;
using com.fdt.genericdatasaving;
using UnityEngine;

namespace com.fdt.statemachinegraph
{
    public class SMContentAsset : ScriptableObjectWithDescription, IParameterProvider
    {
        public virtual string AdditionalFolderData
        {
            get { return string.Empty; }
        }
        
        [SerializeField] protected bool _timed = false;
        

        public bool TimedRange
        {
            get { return false; }
        }

        [SerializeField] protected Color32 _backColor;
        public Color32 BackColor
        {
            get { return _backColor; }
        }

        private void Reset()
        {
            _backColor = new Color32(200,200,200,255);
        }
        
        protected virtual void OnEnable()
        {

        }
        public virtual IContentParameter[] Parameters
        {
            get { return null; }
        }
        
        public virtual void ExitExecutionRange(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, ValueData[] _valueDatas,
            TimeRangeData timeRange, float time, float nTime)
        {

        }
        public virtual void EnterExecutionRange(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, ValueData[] _valueDatas,
            TimeRangeData timeRange, float time, float nTime)
        {

        }

        public virtual void EnterNode(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, ValueData[] cdValues, 
            TimeRangeData cdTimeRange, float time, float nTime)
        {

        }

        public virtual void ExitNode(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, ValueData[] cdValues, 
            TimeRangeData cdTimeRange, float time, float nTime)
        {
           
        }
    }
}