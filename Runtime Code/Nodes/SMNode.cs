﻿using System.Collections.Generic;
using UnityEngine;

namespace com.fdt.statemachinegraph
{
    [NodeWidth(234), NodeTint	(0.5f, 0.5f, 0.5f)]
    public class SMNode : SMTimeNode
    {
        public virtual bool HasConditions
        {
            get { return true; }
        }
        public virtual bool HasSettings
        {
            get { return true; }
        }
        
        protected enum FixedUpdateExecute
        {
            BeforeConditions, AfterConditions
        }

        protected Dictionary<int, SMNodeBase> cache = new Dictionary<int, SMNodeBase>();
        #if UNITY_EDITOR
        protected Dictionary<string, ConditionData> conditionCache = new Dictionary<string, ConditionData>();
        #endif
        public SettingData[] settings;
        [Output(dynamicPortList = true)] public ConditionData[] conditions;
        
        protected override void Cache()
        {
            cache.Clear();
            #if UNITY_EDITOR
            conditionCache.Clear();
            #endif
            foreach (var o in Ports)
            {
                if (o.fieldName.Contains("conditions "))
                {
                    string n = o.fieldName.Replace("conditions ", string.Empty);
                    int idx = -1;
                    if (int.TryParse(n, out idx))
                    {
                        if (idx >= 0)
                        {
                			var c = o.GetConnections();
                			if (c.Count > 0 && c[0].node != null)
                			{
                    			var node = c[0].node as SMNodeBase;
 								cache.Add(idx, node);
                			}
                            else
                            {
                                cache.Add(idx, null);
                            }
                            #if UNITY_EDITOR
                            conditionCache.Add(o.fieldName, conditions[idx]);
                            #endif
                        }
                    }
                }
            }
        }
#if UNITY_EDITOR
        public ConditionData GetCondition(string portName)
        {
            if (conditionCache.ContainsKey(portName))
                return conditionCache[portName];
            else
                return null;
        }
#endif
        public override SMNodeBase GetNextNode(ISMPlayer cdata)
        {
            float time = (Time.time - cdata.nodeInitTime);
            float nTime = time / _maxTime;

            if (cdata is IContentPlayer)
            {
                var resultIdx = CheckConditions(cdata, time, nTime);
                if (resultIdx != -1)
                {
                    return cache[resultIdx].GetNode();
                }
            }
            return base.GetNextNode(cdata);
        }
        // Return the correct value of an output port when requested
        public override int CheckConditions(ISMPlayer cdata, float time, float nTime)
        {
            if (!HasConditions)
                return -1;
            
            // returns the index of the node to go, or -1 to not go to any node
            int result = -1;
            for (int i = 0; i < conditions.Length; i++)
            {
                if (cache[i] == null) continue;
                
                if (!conditions[i].enabled) continue;
                
                if (cache[i] is SMConditionsNode)
                {
                    SMConditionsNode c = cache[i] as SMConditionsNode;
                    bool r = c.CheckConditions(this, cdata, conditions[i].TimeRangeData, time, nTime);
                    if (r)
                    {
                        result = i;
                        break;
                    }
                }
                else if (conditions[i].condition != null)
                {
                    // normal condition
                    if (CheckCondition(i, conditions[i], cdata, conditions[i].TimeRangeData, time, nTime))
                    {
                        result = i;
                        break;
                    }
                }
            }
            return result;
        }

        
        public override void FixedUpdateAfterConditions(ISMPlayer data, float time)
        {
            base.FixedUpdateAfterConditions(data, time);
            
            if (!HasSettings)
                return;
            
            if (!(data is IContentPlayer))
                return;
            
            float nTime = time / _maxTime;
            SettingData s = null;
            for (int i = 0; i < settings.Length; i++)
            {
                s = settings[i];
                if (s.enabled && s.setting != null)
                {
                    ExecuteSetting(i, s, data, s.TimeRangeData, time, nTime,FixedUpdateExecute.AfterConditions);
                }
            }
        }

        public override void FixedUpdateBeforeConditions(ISMPlayer data, float time)
        {
            base.FixedUpdateBeforeConditions(data, time);
            
            if (!HasSettings)
                return;

            if (!(data is IContentPlayer))
                return;
            
            float nTime = time / _maxTime;

            for (int i = 0; i < settings.Length; i++)
            {
                var s = settings[i];
                if (s.enabled && s.setting != null)
                {
                    ExecuteSetting(i, s, data, s.TimeRangeData, time, nTime, FixedUpdateExecute.BeforeConditions);
                }
            }
        }
        private void ExecuteSetting(int contentIdx, SettingData setting, ISMPlayer cdata, TimeRangeData timeRangeData, float time, float nTime,
            FixedUpdateExecute conditionOrder)
        {
            var settingAsset = setting.setting;
            bool higherTanRange = setting.TimeRangeData.IsHigherTanRange(time, nTime);
            bool executeOnRange = setting.TimeRangeData.IsInRange(time, nTime);
            bool hasBeenActiveInNode = HasSettingEnteredExecutionRange(contentIdx,  cdata);
            bool executeEnterRange = !IsSettingInExecutionRange(contentIdx, cdata) && executeOnRange;
            bool executeExitRange = IsSettingInExecutionRange(contentIdx, cdata) && !executeOnRange;
            if (!hasBeenActiveInNode && higherTanRange)
            {
                executeEnterRange = true;
                executeExitRange = true;
            }
            
            if (executeEnterRange)
            {
                SetSettingOnExecutionRange(contentIdx, cdata);
                DoEnterExecutionRange(contentIdx, settingAsset, setting, cdata, timeRangeData, time, nTime);
            }

            if (executeOnRange)
            {
                if (conditionOrder == FixedUpdateExecute.AfterConditions)
                {
                    settingAsset.ExecuteUpdateAfterFixedUpdate( contentIdx, this, cdata, setting.values,
                        time, nTime);    
                }
                else
                {
                    settingAsset.ExecuteUpdateBeforeFixedUpdate( contentIdx, this, cdata, setting.values,
                        time, nTime);
                }
            }
            else
            {
                if (conditionOrder == FixedUpdateExecute.AfterConditions)
                {
                    settingAsset.ExecuteUpdateAfterFixedUpdateOutside( contentIdx, this, cdata, setting.values,
                        time, nTime);    
                }
                else
                {
                    settingAsset.ExecuteUpdateBeforeFixedUpdateOutside( contentIdx, this, cdata, setting.values,
                        time, nTime);
                }
            }
            if (executeExitRange)
            {
                ((IContentPlayer) cdata).settingsData[contentIdx] = false;
                DoExitExecutionRange(contentIdx, settingAsset, setting, cdata, timeRangeData, time, nTime);
            }
        }


        public override void Exit(ISMPlayer cdata, float time)
        {
            float nTime = time / _maxTime;
            if (cdata is IContentPlayer)
            {
                IContentPlayer cpdata = cdata as IContentPlayer;
                if (HasSettings)
                {
                    for (int i = 0; i < settings.Length; i++)
                    {
                        var s = settings[i];
                        if (s.enabled && s.setting != null)
                        {
                            if (IsSettingInExecutionRange(i, cdata))
                            {
                                (cdata as IContentPlayer).settingsData[i] = false;
                                DoExitExecutionRange(i, s.setting, s, cdata, s.TimeRangeData, time, nTime);
                            }

                            DoExitNode(i, s.setting, s, cdata, s.TimeRangeData, time, nTime);
                        }
                    }
                }

                if (HasConditions)
                {
                    for (int i = 0; i < conditions.Length; i++)
                    {
                        if (cache[i] == null) continue;

                        var condition = conditions[i];
                        
                        if (!condition.enabled) continue;

                        if (!(cache[i] is SMConditionsNode))
                        {
                            if (IsConditionInExecutionRange(i, cdata))
                            {
                                cpdata.conditionsData[i] = false;
                                DoExitExecutionRange(i, condition.condition, condition, cdata, condition.TimeRangeData, time,
                                    nTime);
                            }

                            DoExitNode(i, condition.condition, condition, cdata, condition.TimeRangeData, time, nTime);
                        }
                        else
                        {
                            SMConditionsNode c = cache[i] as SMConditionsNode;
                            c.Exit(cdata, condition.TimeRangeData, time, nTime);
                        }
                    }
                }
                for (int i = 0; i < settings.Length; i++)
                {
                    cpdata.settingsData[i] = false;
                    cpdata.settingsDataAny[i] = false;
                }
                for (int i = 0; i < conditions.Length; i++)
                {
                    cpdata.conditionsData[i] = false;
                    cpdata.conditionsDataAny[i] = false;
                }
            }
        }

        public override void Enter(ISMPlayer cdata)
        {
            float time = (Time.time - cdata.nodeInitTime);
            float nTime = time / _maxTime;
            if (cdata is IContentPlayer)
            {
                IContentPlayer cpdata = cdata as IContentPlayer;
                for (int i = 0; i < settings.Length; i++)
                {
                    cpdata.settingsData[i] = false;
                    cpdata.settingsDataAny[i] = false;
                }
                for (int i = 0; i < conditions.Length; i++)
                {
                    cpdata.conditionsData[i] = false;
                    cpdata.conditionsDataAny[i] = false;
                }
                
                if (HasSettings)
                {
                    for (int i = 0; i < settings.Length; i++)
                    {
                        var s = settings[i];
                        if (s.enabled && s.setting != null)
                        {
                            DoEnterNode(i, s.setting, s, cdata, s.TimeRangeData, time, nTime);
                            bool executeOnRange = s.TimeRangeData.IsInRange(time, nTime);
                            if (executeOnRange)
                            {
                                SetSettingOnExecutionRange(i, cdata);
                                DoEnterExecutionRange(i, s.setting, s, cdata, s.TimeRangeData, time, nTime);
                            }
                        }
                    }
                }

                if (HasConditions)
                {
                    for (int i = 0; i < conditions.Length; i++)
                    {
                        if (cache[i] == null) continue;

                        var condition = conditions[i];
                        
                        if (!condition.enabled) continue;
                        
                        if (!(cache[i] is SMConditionsNode) && condition.condition != null)
                        {
                            DoEnterNode(i, condition.condition, condition, cdata, condition.TimeRangeData, time, nTime);
                            bool executeOnRange = condition.TimeRangeData.IsInRange(time, nTime);
                            if (executeOnRange)
                            {
                                SetConditionOnExecutionRange(i, cdata);
                                DoEnterExecutionRange(i, condition.condition, condition, cdata, condition.TimeRangeData,
                                    time,
                                    nTime);
                            }
                        }
                        else if (cache[i] is SMConditionsNode)
                        {
                            SMConditionsNode c = cache[i] as SMConditionsNode;
                            c.Enter(cdata, condition.TimeRangeData, _maxTime);
                        }
                    }
                }
                
            }
        }
    }
}