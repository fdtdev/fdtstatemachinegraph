﻿using UnityEngine;

namespace com.fdt.statemachinegraph.common
{
    public class SMGoToNode : SMNodeBase
    {
        protected SMNodeBase nextNode = null;
        [Input] public Object enter;
        #if UNITY_EDITOR
        [SerializeField] protected SMGraph _graph;
        #endif
        [SerializeField] protected SMNodeBase _node;
        
        protected override void Cache()
        {
            
        }

        public override SMNodeBase GetNextNode(ISMPlayer cdata)
        {
            return _node;
        }
    }
}