﻿using XNode;
using UnityEngine;

namespace com.fdt.statemachinegraph
{
    public abstract class SMNodeBase : Node
    {
        protected Vector2 fullTime = new Vector2(0, 1);
        
        protected override void Init()
        {
            base.Init(); 
            Cache();
        }

        protected virtual void Cache()
        {

        }
        public override object GetValue(NodePort port)
        {
            return null;
        }
        public virtual SMNodeBase GetNextNode(ISMPlayer cdata)
        {
            return null;
        }
        public virtual int CheckConditions(ISMPlayer cdata, float time, float nTime)
        {
            return -1;
        }

        public virtual void SetNode(ISMPlayer data)
        {
            
        }
        public virtual void FixedUpdateBeforeConditions(ISMPlayer data, float time)
        {
            
        }
        public virtual void FixedUpdateAfterConditions(ISMPlayer data, float time)
        {

        }
        public virtual void Exit(ISMPlayer cdata, float time)
        {
            
        }
        public virtual void Enter(ISMPlayer cdata)
        {
            
        }

        public virtual SMNodeBase GetNode()
        {
            return this;
        }
        
        protected bool CheckCondition(int contentIdx, ConditionData condition, ISMPlayer cdata, TimeRangeData timeRange, float time, float nTime)
        {
            var conditionAsset = condition.condition;
            if (timeRange.tType == TimeRangeData.TimedType.ABSOLUTE || timeRange.tType == TimeRangeData.TimedType.RELATIVE)
            {
                Debug.LogError("ABSOLUTE or RELATIVE not allowed");
                return false;
            }
            else
            {
                bool higherTanRange = timeRange.IsHigherTanRange(time, nTime);
                bool executeOnRange = timeRange.IsInRange(time, nTime);
                bool hasBeenActiveInNode = HasConditionEnteredExecutionRange(contentIdx, cdata);
                bool insideExecutionRange = IsConditionInExecutionRange(contentIdx, cdata);
            
                bool executeEnterRange = !insideExecutionRange && executeOnRange;
                bool executeExitRange = insideExecutionRange && !executeOnRange;
                
                if (!hasBeenActiveInNode && higherTanRange)
                {
                    executeEnterRange = true;
                    executeExitRange = true;
                }
                
                if (executeEnterRange)
                {
                    SetConditionOnExecutionRange(contentIdx, cdata);
                    DoEnterExecutionRange(contentIdx, conditionAsset, condition, cdata, timeRange, time, nTime);
                    executeOnRange = true;
                    executeEnterRange = !insideExecutionRange && executeOnRange;
                    executeExitRange = insideExecutionRange && !executeOnRange;
                }

                bool result = false;
          
                if (executeOnRange || executeExitRange)
                {
                    bool r = conditionAsset.CheckCondition(contentIdx, this, cdata, condition.values,
                        timeRange, time,
                        nTime);
                    if (r)
                    {
                        result = true;
                    }
                }
                else if (!executeOnRange)
                {
                    bool r = conditionAsset.CheckConditionOutside(contentIdx, this, cdata, condition.values,
                        timeRange, time,
                        nTime);
                    if (r)
                    {
                        result = true;
                    }
                }

                if (executeExitRange)
                {
                    var cpdata = cdata as IContentPlayer;
                    cpdata.conditionsData[contentIdx] = false;
                    DoExitExecutionRange(contentIdx, conditionAsset, condition, cdata, timeRange, time, nTime);
                }
                return result;
            }
          
        }

        protected void SetConditionOnExecutionRange(int contentIdx, ISMPlayer cdata)
        {
            var cpdata = cdata as IContentPlayer;
            cpdata.conditionsData[contentIdx] = true;
            cpdata.conditionsDataAny[contentIdx] = true;
        }
        protected void SetSettingOnExecutionRange(int contentIdx, ISMPlayer cdata)
        {
            var cpdata = cdata as IContentPlayer;
            cpdata.settingsData[contentIdx] = true;
            cpdata.settingsDataAny[contentIdx] = true;
        }
        protected void DoEnterExecutionRange(int contentIdx, SMContentAsset c, ContentData cd, ISMPlayer cdata, TimeRangeData timeRangeData, float time,
            float nTime)
        {
            c.EnterExecutionRange(contentIdx, this, cdata, cd.values, timeRangeData, time, nTime);
        }
        protected void DoEnterNode(int contentIdx, SMContentAsset c, ContentData cd, ISMPlayer cdata, TimeRangeData timeRangeData, float time,
            float nTime)
        {
            c.EnterNode( contentIdx, this, cdata, cd.values, timeRangeData, time, nTime);
        }
        protected void DoExitExecutionRange(int contentIdx, SMContentAsset c, ContentData cd, ISMPlayer cdata, TimeRangeData timeRangeData, float time,
            float nTime)
        {
            c.ExitExecutionRange(contentIdx, this, cdata, cd.values, timeRangeData, time, nTime);
        }
        
        protected void DoExitNode(int contentIdx, SMContentAsset c, ContentData cd, ISMPlayer cdata, TimeRangeData timeRangeData, float time,
            float nTime)
        {
            c.ExitNode(contentIdx, this, cdata, cd.values, timeRangeData, time, nTime);
        }
        
        
        protected bool HasSettingEnteredExecutionRange(int idx, ISMPlayer cdata)
        {
            return ((IContentPlayer) cdata).settingsDataAny[idx];
        }
        protected bool IsSettingInExecutionRange(int idx, ISMPlayer cdata)
        {
            return ((IContentPlayer) cdata).settingsData[idx];
        }
        protected bool HasConditionEnteredExecutionRange(int idx, ISMPlayer cdata)
        {
            return ((IContentPlayer) cdata).conditionsDataAny[idx];
        }
        protected bool IsConditionInExecutionRange(int idx, ISMPlayer cdata)
        {
            return ((IContentPlayer) cdata).conditionsData[idx];
        }
    }
}