﻿using System;
using com.fdt.genericdatasaving;
using UnityEngine;

namespace com.fdt.statemachinegraph
{
    [NodeWidth(234)]
    public class SMConditionsNode : SMNodeBase
    {
        [Input] public UnityEngine.Object enter;
        public ConditionData[] conditions;
        
        [Output(ShowBackingValue.Never, ConnectionType.Override)]
        public UnityEngine.Object output;

        protected SMNodeBase _cachedNextNode;

        protected readonly string outputPortName = "output";
        protected override void Cache()
        {
            base.Cache();

            if (HasPort(outputPortName))
            {
                var c = GetOutputPort(outputPortName).GetConnections();
                if (c.Count > 0 && c[0].node != null)
                {
                    _cachedNextNode = c[0].node as SMNodeBase;
                }
            }
        }
        public virtual bool CheckConditions(SMNode n, ISMPlayer cdata,
            TimeRangeData timeRange, float time, float nTime)
        {
            if (conditions.Length > 0)
            {
                for (int i = 0; i < conditions.Length; i++)
                {
                    if (!CheckCondition(i, conditions[i], cdata, timeRange, time, nTime))
                        return false;
                }
                return true;
            }
            return false;
        }
        public override SMNodeBase GetNode()
        {
            return _cachedNextNode;
        }
        public virtual void Enter(ISMPlayer cdata, TimeRangeData timeRangeData, float maxTime)
        {
            float time = (Time.time - cdata.nodeInitTime);
            float nTime =  time / maxTime;

            for(int i = 0; i < conditions.Length; i++)
            {
                var condition = conditions[i];

                DoEnterNode(i, condition.condition, condition, cdata, timeRangeData, 0, 0);
                bool executeOnRange = timeRangeData.IsInRange(time, nTime);
                if (executeOnRange)
                {
                    SetConditionOnExecutionRange(i, cdata);
                    DoEnterExecutionRange(i, condition.condition, condition, cdata, timeRangeData, 0, 0);
                }
            }
        }
        
        
        public virtual void Exit(ISMPlayer cdata, TimeRangeData timeRangeData, float time, float nTime)
        {
            for(int i = 0; i < conditions.Length; i++)
            {
                var condition = conditions[i];
                if (IsConditionInExecutionRange(i, cdata))
                {
                    ((IContentPlayer) cdata).conditionsData[i] = false;
                    DoExitExecutionRange(i, condition.condition, condition, cdata, timeRangeData, time, nTime);
                }
                DoExitNode(i, condition.condition, condition, cdata, timeRangeData, time, nTime);
            }
        }
    }
}