﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.fdt.statemachinegraph
{
    [NodeWidth(150), NodeTint	(0.1f, 0.55f, 0.1f)]
    public class SMTimeNode : SMNodeBase
    {
        public virtual bool HasTime
        {
            get { return true; }
        }
        [SerializeField] protected float _maxTime = 1;
        
        [Input(ShowBackingValue.Never, ConnectionType.Multiple)] public Object enter;
        [Output(ShowBackingValue.Never, ConnectionType.Override)] public Object exit;
        
        public override void FixedUpdateAfterConditions(ISMPlayer data, float time)
        {            
            float nTime = time / _maxTime;
            FixedUpdateAfterConditions(data, time, nTime);
        }

        public override void FixedUpdateBeforeConditions(ISMPlayer data, float time)
        {
            float nTime = time / _maxTime;
            FixedUpdateBeforeConditions(data, time, nTime);
        }
        protected virtual void FixedUpdateAfterConditions(ISMPlayer data, float time, float nTime)
        {
            
        }
        protected virtual void FixedUpdateBeforeConditions(ISMPlayer data, float time, float nTime)
        {
            
        }
        public override SMNodeBase GetNextNode(ISMPlayer cdata)
        {
            
            float time = (Time.time - cdata.nodeInitTime);
            float nTime = time / _maxTime;

            if (HasTime)
            {
                if (nTime > 1.0f)
                {
                    var p = GetPort("exit");
                    if (p.ConnectionCount > 0)
                    {
                        return ((SMNodeBase) p.GetConnection(0).node).GetNode();
                    }
                }
            }

            return null;
        }
    }
}