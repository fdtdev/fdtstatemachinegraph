﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SMPlayType
{
    FIXEDUPD = 0, UPD = 1, MANUAL = 2, FIXEDUPD_DET = 3, UPD_DET = 4, MANUAL_DET = 5
}
