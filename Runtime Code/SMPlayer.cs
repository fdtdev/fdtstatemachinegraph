﻿using System;
using com.fdt.genericdatasaving;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.fdt.statemachinegraph
{
    public class SMPlayer : GenericDataSave, IContentPlayer
    {
        [SerializeField] protected SMPlayUtil _playerData; 
        public SMNodeBase currentNode
        {
            get => _playerData.currentNode;
            set => _playerData.currentNode = value;
        }
        public float nodeInitTime 
        {
            get => _playerData.nodeInitTime;
            set => _playerData.nodeInitTime = value;
        }
        [SerializeField] protected bool _autoPlay = false;

        protected virtual void OnEnable()
        {
            if (_autoPlay)
            {
                _playerData.Play(this);
            }
        }
        protected virtual void OnDisable()
        {
            
        }
        public void Play()
        {
            _playerData.Play(this);
        }

        private void FixedUpdate()
        {
            _playerData.FixedUpdate();
        }
        private void Update()
        {
            _playerData.Update();
        }
        public void SetNode(SMNodeBase n)
        {
            _playerData.SetNode(this, n);
        }

        protected bool[] _settingsData = new bool[32];
        public bool[] settingsData
        {
            get { return _settingsData; }
        }
        protected bool[] _settingsDataAny = new bool[32];
        public bool[] settingsDataAny
        {
            get { return _settingsDataAny; }
        }
        protected bool[] _conditionsData = new bool[32];
        public bool[] conditionsData
        {
            get { return _conditionsData; }
        }
        protected bool[] _conditionsDataAny = new bool[32];
        public bool[] conditionsDataAny
        {
            get { return _conditionsDataAny; }
        }
    }
}