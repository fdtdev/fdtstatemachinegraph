﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.fdt.statemachinegraph
{
    [System.Serializable]
    public class SMPlayUtil
    {
        [SerializeField] protected SMNodeBase _currentNode;
        [SerializeField] protected float _nodeInitTime;
        [SerializeField] protected float updateStepTime = 0.015f;
        [SerializeField] protected float fixedUpdateStepTime = 0.02f;
        [SerializeField] protected SMPlayType _playType;
        protected ISMPlayer _player;
        protected bool _playing = false;
        protected float currentTime;
        protected float targetTime;
        protected float lastTime;

        protected SMPlayType currentPlayType;

        public SMNodeBase currentNode
        {
            get => _currentNode;
            set => _currentNode = value;
        }
        public float nodeInitTime 
        {
            get => _nodeInitTime;
            set => _nodeInitTime = value;
        }
        public void Play(ISMPlayer player)
        {
            _playing = true;
            currentPlayType = _playType;
            Init(player, currentNode);
        }
        void Init(ISMPlayer player, SMNodeBase n)
        {
            _player = player;
            currentNode = n;
            nodeInitTime = Time.time;
            currentNode.SetNode(_player);
            currentNode.Enter(_player);
        }
        public void FixedUpdate()
        {
            if (!_playing)
                return;
            
            if (currentPlayType == SMPlayType.FIXEDUPD || currentPlayType == SMPlayType.FIXEDUPD_DET)
            {
                if (currentPlayType == SMPlayType.FIXEDUPD_DET)
                {
                    ExecuteDeterministicSteps(fixedUpdateStepTime);
                }
                else
                {
                    targetTime = (Time.time - nodeInitTime);
                    currentTime = targetTime;
                    ExecuteStep();
                    lastTime = targetTime;
                }
            }
        }

        public void Update()
        {
            if (!_playing)
                return;
            if (currentPlayType == SMPlayType.UPD || currentPlayType == SMPlayType.UPD_DET)
            {
                if (currentPlayType == SMPlayType.UPD_DET)
                {
                    ExecuteDeterministicSteps(updateStepTime);
                }
                else
                {
                    targetTime = (Time.time - nodeInitTime);
                    currentTime = targetTime;
                    ExecuteStep();
                    lastTime = targetTime;
                }
            }
        }
        protected virtual void ExecuteDeterministicSteps(float fdt)
        {
            bool executeSingle = true;

            float cTime = (Time.time - nodeInitTime);
            float dif = cTime - lastTime;
            if (dif > fdt)
            {
                executeSingle = false;
                currentTime = lastTime;
                targetTime = cTime;
                do
                {
                    currentTime += fdt;
                    ExecuteStep();

                } while (currentTime < (targetTime - fdt));

                if (currentTime < targetTime)
                {
                    currentTime = targetTime;
                    ExecuteStep();
                }
            }

            if (executeSingle)
            {
                targetTime = (Time.time - nodeInitTime);
                currentTime = cTime;
                ExecuteStep();
            }

            lastTime = targetTime;
        }
        private void ResetTime()
        {
            targetTime = (Time.time - nodeInitTime);
            currentTime = targetTime;
        }
        protected virtual void ExecuteStep()
        {
            if (currentNode != null)
                currentNode.FixedUpdateBeforeConditions(_player, currentTime);

            var n = currentNode.GetNextNode(_player);
            if (n != null)
            {
                if (currentNode != null)
                {
                    currentNode.Exit(_player, currentTime);
                }

                Init(_player, n);
                ResetTime();
            }
            

            if (currentNode != null)
                currentNode.FixedUpdateAfterConditions(_player, currentTime);
        }

        public void SetNode(ISMPlayer smPlayer, SMNodeBase n)
        {
            _player = smPlayer;
            if (n == null)
            {
                Debug.LogError("Tried to change to null node");
            }
            if (currentNode != null)
            {
                currentNode.Exit(_player, currentTime);
            }

            Init(_player, n);
            ResetTime();
        }
    }
}