# FDT State Machine Graph

xNode based state machine


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.statemachinegraph": "https://bitbucket.org/fdtdev/fdtstatemachinegraph.git#3.0.0",
	"com.fdt.common": "https://bitbucket.org/fdtdev/fdtcommon.git#5.0.0",
	"com.fdt.genericdatasaving": "https://bitbucket.org/fdtdev/fdtgenericdatasaving.git#4.0.0",
	"com.fdt.gameevents": "https://bitbucket.org/fdtdev/fdtgameevents.git#2.0.0",
	"com.github.siccity.xnode": "https://github.com/siccity/xNode.git"

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtstatemachinegraph/src/3.0.0/LICENSE.md)