﻿using UnityEditor;
using UnityEngine;

namespace com.fdt.statemachinegraph.editor
{
    [CustomPropertyDrawer(typeof(TimeRangeData))]
    public class TimeRangeDataDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            float labelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = 110;

            EditorGUI.BeginProperty(position, label, property);
            GUI.Box(position, GUIContent.none);
            position.x += 2;
            position.width -= 4;
            position.y += 2;
            position.height -= 4;

            SerializedProperty rType = property.FindPropertyRelative("tType");
            EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, 16), rType, new GUIContent("Timing Type"));

            TimeRangeData.TimedType t = (TimeRangeData.TimedType) rType.intValue;
            switch (t)
            {
                case TimeRangeData.TimedType.RANGE:
                    SerializedProperty timeRangeProp = property.FindPropertyRelative("timeRange");
                    EditorGUI.PropertyField(new Rect(position.x, position.y + 18, position.width, 16), timeRangeProp);
                    if (timeRangeProp.vector2Value.y == 1)
                    {
                        EditorGUI.PropertyField(new Rect(position.x, position.y + 36, position.width, 16),
                            property.FindPropertyRelative("extrapolate"));
                    }
                    break;
                case TimeRangeData.TimedType.RELATIVE:
                    EditorGUI.PropertyField(new Rect(position.x, position.y + 18, position.width, 16), property
                        .FindPropertyRelative("normalizedTime"));
                    break;
                case TimeRangeData.TimedType.ABSOLUTE:
                    EditorGUI.PropertyField(new Rect(position.x, position.y + 18, position.width, 16), property
                        .FindPropertyRelative("seconds"));
                    break;

            }

            EditorGUI.EndProperty();
            EditorGUIUtility.labelWidth = labelWidth;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty rType = property.FindPropertyRelative("tType");
            TimeRangeData.TimedType t = (TimeRangeData.TimedType) rType.intValue;
            switch (t)
            {
                case TimeRangeData.TimedType.RANGE:
                    SerializedProperty timeRangeProp = property.FindPropertyRelative("timeRange");
                    if (timeRangeProp.vector2Value.y == 1)
                    {
                        return 58;
                    }
                    else
                    {
                        return 40;
                    }

                case TimeRangeData.TimedType.NONE:
                    return 22;
                case TimeRangeData.TimedType.ABSOLUTE:
                case TimeRangeData.TimedType.RELATIVE:
                    return 40;
            }

            return base.GetPropertyHeight(property, label) + 4;
        }
    }
}