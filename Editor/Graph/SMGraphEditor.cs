﻿using System;
using System.Linq;
using com.FDT.Common.RotorzReorderableList;
using UnityEditor;
using UnityEngine;
using XNode;
using XNodeEditor;

namespace com.fdt.statemachinegraph.editor
{
    [CustomEditor(typeof(SMGraph), true)]
    public class SMGraphInspector : Editor
    {
        protected bool error = false;
        private void OnEnable()
        {
            error = false;
            var nodes = (target as SMGraph).nodes;
            for (int i = 0; i < nodes.Count; i++)
            {
                if (nodes[i] == null)
                {
                    error = true;
                    break;
                }
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            var cTarget = target as SMGraph;
            if (error)
            {
                EditorGUILayout.HelpBox("Error in serialization", MessageType.Error);
            }

            ReorderableListGUI.Title("Enabled Namespaces");
            cTarget.CreateCachedNamespaces();
            GUI.enabled = false;
            ReorderableListGUI.ListField(serializedObject.FindProperty("_defaultNodeNamespaces"), ReorderableListFlags.DisableReordering|ReorderableListFlags.HideAddButton|ReorderableListFlags.HideRemoveButtons);
            GUI.enabled = true;
            EditorGUI.BeginChangeCheck();
            ReorderableListGUI.ListField(serializedObject.FindProperty("_additionalNodeNamespaces"));
            if (EditorGUI.EndChangeCheck())
            {
                serializedObject.ApplyModifiedProperties();
                EditorUtility.SetDirty(serializedObject.targetObject);
                cTarget.changed = true;
                //cTarget.ResetOptions();
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
    [CustomNodeGraphEditor(typeof(SMGraph))]
    public class SMGraphEditor : NodeGraphEditor
    {
        public static ISMPlayer smPlayer;
        private static GameObject lastSelected;
        protected static Gradient _disabledGradient;
        protected static Gradient disabledGradient
        {
            get
            {
                if (_disabledGradient == null)
                {
                    _disabledGradient = new Gradient();

                    // Populate the color keys at the relative time 0 and 1 (0 and 100%)
                    var colorKey = new GradientColorKey[2];
                    colorKey[0].color = Color.black;
                    colorKey[0].time = 0.0f;
                    colorKey[1].color = Color.black;
                    colorKey[1].time = 1.0f;

                    // Populate the alpha  keys at relative time 0 and 1  (0 and 100%)
                    var alphaKey = new GradientAlphaKey[2];
                    alphaKey[0].alpha = 1.0f;
                    alphaKey[0].time = 0.0f;
                    alphaKey[1].alpha = 1.0f;
                    alphaKey[1].time = 1.0f;

                    _disabledGradient.SetKeys(colorKey, alphaKey);
                }

                return _disabledGradient;
            }
        }
        public override Gradient GetNoodleGradient(NodePort output, NodePort input)
        {
            if (Application.isPlaying && output.node is SMNode && (output.node as SMNode).HasPort(output.fieldName))
            {
                var d = (output.node as SMNode).GetCondition(output.fieldName);
                if (d != null && !d.enabled)
                {
                    return disabledGradient;
                }
            }
            return base.GetNoodleGradient(output, input);
        }

        public override string GetNodeMenuName(Type type)
        {
            SMGraph cTarget = target as SMGraph;
            if (cTarget.additionalNodeNamespaces == null)
                cTarget.CreateCachedNamespaces();
            if (cTarget.defaultNodeNamespaces.Contains(type.Namespace) || cTarget.additionalNodeNamespaces.Contains(type.Namespace))
            {
                //string r = base.GetNodeMenuName(type);
                string r = type.Namespace.ToString() + "/" + type.Name;
                return r;
            }
            return null;
        }

        public override void OnGUI()
        {
            base.OnGUI();
            if (Application.isPlaying)
            {
                if (Selection.activeGameObject != null && Selection.activeGameObject != lastSelected)
                {
                    var s = Selection.activeGameObject;
                    var g = s.GetComponentInChildren<ISMPlayer>();
                    if (g != null)
                    {
                        smPlayer = g;
                    }

                    lastSelected = s;
                }
                if (smPlayer != null)
                {
                    NodeEditorWindow.RepaintAll();
                }
            }
        }
    }
}