﻿using System;
using com.fdt.genericdatasaving;
using UnityEditor;
using UnityEngine;
using XNodeEditor;
using Object = UnityEngine.Object;

namespace com.fdt.statemachinegraph.editor
{
    public class ContentDataDrawer<T> : PropertyDrawer where T: class, IParameterProvider
    {
        protected virtual System.Type GetContentType
        {
            get { return typeof(Object); }
        }
        protected virtual string contentPropName
        {
            get { return string.Empty; }
        }

        private SerializedProperty contentProp;
        private bool hasContentAsset;
        IParameterProvider settingsObject = default(IParameterProvider);
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            SerializedProperty enabledProp = property.FindPropertyRelative("enabled");
            enabledProp.boolValue = EditorGUI.Toggle(new Rect(position.x, position.y + (position.height/2) - 10 , 15, 20), GUIContent.none, enabledProp.boolValue);
            position.x += 15;
            position.width -= 15;
            bool oldEnabled = GUI.enabled;
            GUI.enabled = enabledProp.boolValue;
            var descriptionProp = property.FindPropertyRelative("description");
            property.isExpanded = EditorGUI.Foldout(new Rect(position.x, position.y, 10, 16), property.isExpanded,
                GUIContent.none);
            position.x += 10;
            position.width -= 10;
            float totalHeight = 32;

            contentProp = property.FindPropertyRelative(contentPropName);
            hasContentAsset = contentProp.objectReferenceValue != null;
            
            if (hasContentAsset)
            {
                settingsObject = (contentProp.objectReferenceValue as T);
            }
            Color32 oldColor = GUI.backgroundColor;
            if (settingsObject != null)
            {
                int len = settingsObject.Parameters!=null? settingsObject.Parameters.Length:0;
                totalHeight += (len * 18);
                GUI.backgroundColor = settingsObject.BackColor;
            }
            GUI.Box(position, GUIContent.none);
            
            Rect r3 = new Rect(position.x, position.y, position.width, 16);
                
            EditorGUI.PropertyField(r3, descriptionProp);
            r3.y += 18;
            EditorGUI.BeginChangeCheck();
            Color oldC = GUI.backgroundColor;
            if (contentProp.objectReferenceValue == null)
            {
                GUI.backgroundColor = Color.red;
            }
            EditorGUI.PropertyField(r3, contentProp);
            GUI.backgroundColor = oldC;
            
            if (EditorGUI.EndChangeCheck())
            {
                ApplyChanges(property);
                hasContentAsset = contentProp.objectReferenceValue != null;
                settingsObject = (contentProp.objectReferenceValue as T);
            }
            SerializedProperty valuesProp = property.FindPropertyRelative("values");
            
            if (hasContentAsset)
            {
                settingsObject = (contentProp.objectReferenceValue as T);
                
                int len = settingsObject.Parameters!=null? settingsObject.Parameters.Length:0;
                if (valuesProp.arraySize < len)
                {
                    valuesProp.arraySize = len;
                    ApplyChanges(property);
                    hasContentAsset = contentProp.objectReferenceValue != null;
                    settingsObject = (contentProp.objectReferenceValue as T);
                }

                SerializedProperty trd = property.FindPropertyRelative("_timeRangeData");
                 
                var trdheight = EditorGUI.GetPropertyHeight(trd);
                r3.y += 18;
                float oHeight = r3.height;
                r3.height = trdheight;
                EditorGUI.PropertyField(r3, trd, true);
                r3.height = oHeight;
                totalHeight += trdheight;
                r3.y += (trdheight - 18);

                totalHeight += (len * 18);
                
                int i2 = -1;
                for (int i = 0; i < len; i++)
                {
                    i2++;
                    var param1 = settingsObject.Parameters[i2];
                    var VarType = param1.GetVarType;
                    var AssetType = param1.GetAssetType;
                    var FromAsset = param1.GetTypeFromAsset;
                    var descr = param1.GetCustomDescription;
                    var valueProp = valuesProp.GetArrayElementAtIndex(i);
                    var valueTypeProp = valueProp.FindPropertyRelative("_varType");
                    
                    if (VarType != VarType.NOT_SET)
                    {
                        
                        if (valueTypeProp.intValue != (int) VarType)
                        {
                            valueTypeProp.intValue = (int) VarType;
                            ApplyChanges(property);
                        }
                    }

                    r3.y += 18;
                    
                    string lbl = string.Empty;
                    if (!string.IsNullOrEmpty(descr))
                    {
                        lbl = descr;    
                    }
                    if (VarType == VarType.ASSET)
                    {
                        SerializedProperty value1AssetProp = null;
                        value1AssetProp = valueProp.FindPropertyRelative("_assetValue");
                        if (string.IsNullOrEmpty(descr))
                        {
                            lbl = "Asset";    
                        }
                        
                        value1AssetProp.objectReferenceValue = EditorGUI.ObjectField(r3,
                            new GUIContent(lbl),
                            value1AssetProp.objectReferenceValue, AssetType, false);

                        if (FromAsset && value1AssetProp.objectReferenceValue != null && value1AssetProp.objectReferenceValue is IParameterProvider)
                        {
                            settingsObject = value1AssetProp.objectReferenceValue as IParameterProvider;
                            len += settingsObject.Parameters.Length;
                            if (valuesProp.arraySize < len)
                            {
                                valuesProp.arraySize = len;
                                ApplyChanges(property);
                            }
                            i2 = -1;
                        }
                    }
                    else if (VarType == VarType.UOBJECT)
                    {
                        SerializedProperty value1AssetProp = null;
                        
                        value1AssetProp = valueProp.FindPropertyRelative("_uObjectValue");
                        if (string.IsNullOrEmpty(descr))
                        {
                            lbl = "uObject";    
                        }
                        
                        value1AssetProp.objectReferenceValue = EditorGUI.ObjectField(r3,
                            new GUIContent(lbl),
                            value1AssetProp.objectReferenceValue, AssetType, false);
                    }
                    else if (VarType == VarType.INT && param1.GetAssetType != null && param1.GetAssetType.IsEnum)        // enums
                    {
                        SerializedProperty value1IntProp = null;
                        
                        value1IntProp = valueProp.FindPropertyRelative("_intValue");
                        var enumValue = param1.GetAssetType;

                        if (string.IsNullOrEmpty(descr))
                        {
                            lbl = "EnumValue";    
                        }
                        
                        var t = Enum.GetValues(enumValue);
                        int c = t.Length;
                        string[] options = new string[c];
                        int i3 = 0;
                        foreach (var s in t)
                        {
                            options[i3] = s.ToString();
                            i3++;
                        }
                        var val = EditorGUI.Popup(r3, lbl, value1IntProp.intValue, options);
                        value1IntProp.intValue = val;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(descr))
                        {
                            lbl = null;
                        }
                        EditorGUI.PropertyField(r3, valueProp, new GUIContent(lbl), true);
                    }
                }
                if (valuesProp.arraySize > len)
                {
                    valuesProp.arraySize = len;
                    valuesProp.serializedObject.ApplyModifiedProperties();
                }
            }

            GUI.enabled = oldEnabled;
            GUI.backgroundColor = oldColor;
            if (EditorGUI.EndChangeCheck())
            {
                ClearUnusedData(valuesProp);
                ApplyChanges(property);
                hasContentAsset = contentProp.objectReferenceValue != null;
                settingsObject = (contentProp.objectReferenceValue as T);
            }
            EditorGUI.EndProperty();
        }

        protected void ApplyChanges(SerializedProperty prop)
        {
            prop.serializedObject.ApplyModifiedProperties();
            prop.serializedObject.Update();
            
        }
        protected void ClearUnusedData(SerializedProperty valuesProp)
        {
            int len = valuesProp.arraySize;
            for (int i = 0; i < len; i++)
            {
                var valueProp = valuesProp.GetArrayElementAtIndex(i);
                var valueTypeProp = valueProp.FindPropertyRelative("_varType");
                var VarType = (VarType) valueTypeProp.intValue;

                if (VarType != VarType.UOBJECT)
                {
                    valueProp.FindPropertyRelative("_uObjectValue").objectReferenceValue = null;
                }
                if (VarType != VarType.INT)
                {
                    valueProp.FindPropertyRelative("_intValue").intValue = 0;
                }
                if (VarType != VarType.FLOAT)
                {
                    valueProp.FindPropertyRelative("_floatValue").floatValue = 0;
                }
                if (VarType != VarType.STRING)
                {
                    valueProp.FindPropertyRelative("_stringValue").stringValue = string.Empty;
                }
                if (VarType != VarType.ASSET)
                {
                    valueProp.FindPropertyRelative("_assetValue").objectReferenceValue = null;
                }
                if (VarType != VarType.VECTOR3)
                {
                    valueProp.FindPropertyRelative("_vector3Value").vector3Value = Vector3.zero;
                }
            }
            valuesProp.serializedObject.ApplyModifiedProperties();
        }
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (!property.isExpanded)
                return 18;

            float h = 20;
            SerializedProperty conditionProp = property.FindPropertyRelative(contentPropName);
            h += EditorGUI.GetPropertyHeight(conditionProp, true);

            
            if (conditionProp.objectReferenceValue != null)
            {
                IParameterProvider settingsObject = conditionProp.objectReferenceValue as IParameterProvider;
                int len = settingsObject.Parameters!=null? settingsObject.Parameters.Length:0;
                var valuesProp = property.FindPropertyRelative("values");
                if (valuesProp.arraySize < len)
                {
                    valuesProp.arraySize = len;
                    valuesProp.serializedObject.ApplyModifiedProperties();
                }

                SerializedProperty trd = property.FindPropertyRelative("_timeRangeData");
                var trdheight = EditorGUI.GetPropertyHeight(trd);
                h += trdheight+4;

                int i2 = -1;
                for (int i = 0; i < len; i++)
                {
                    i2++;
                    var param1 = settingsObject.Parameters[i2];
                    var VarType = param1.GetVarType;
                    var AssetType = param1.GetAssetType;
                    var FromAsset = param1.GetTypeFromAsset;

                    var valueProp = valuesProp.GetArrayElementAtIndex(i);
                    var valueTypeProp = valueProp.FindPropertyRelative("_varType");
                    
                    if (VarType == VarType.ASSET)
                    {
                        SerializedProperty value1AssetProp = null;
                        value1AssetProp = valueProp.FindPropertyRelative("_assetValue");

                        if (FromAsset && value1AssetProp.objectReferenceValue != null && value1AssetProp.objectReferenceValue is IParameterProvider)
                        {
                            settingsObject = value1AssetProp.objectReferenceValue as IParameterProvider;
                            len += settingsObject.Parameters.Length;
                            i2 = -1;
                        }
                        h += 18;
                    }
                    else
                    {
                        var c = EditorGUI.GetPropertyHeight(valueProp);
                        h += c;
                    }
                    //h += 18;
                }
            }
            return h;
        }
    }
}