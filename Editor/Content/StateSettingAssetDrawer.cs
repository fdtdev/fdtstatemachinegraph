﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XNode;
using Object = UnityEngine.Object;

namespace com.fdt.statemachinegraph.editor
{
    [CustomPropertyDrawer(typeof(StateSettingAsset))]
    public class StateSettingAssetDrawer : ContentAssetDrawer<StateSettingAsset>
    {
        protected override List<StateSettingAsset> objectsList(SMGraph graph)
        {
            return graph.settingsObjects;
        }

        protected override string[] objectsOptions(SMGraph graph)
        {
            return graph.settingOptions;
        }
    }
}