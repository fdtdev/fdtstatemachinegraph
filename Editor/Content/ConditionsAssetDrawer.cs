﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XNode;
using Object = UnityEngine.Object;

namespace com.fdt.statemachinegraph.editor
{
    [CustomPropertyDrawer(typeof(ConditionsAsset))]
    public class ConditionsAssetDrawer : ContentAssetDrawer<ConditionsAsset>
    {
        protected override List<ConditionsAsset> objectsList(SMGraph graph)
        {
            return graph.conditionObjects;
        }

        protected override string[] objectsOptions(SMGraph graph)
        {
            return graph.conditionOptions;
        }
    }
}