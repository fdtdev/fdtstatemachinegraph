﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace com.fdt.statemachinegraph.editor
{
    public class ContentAssetPostProcessor : AssetPostprocessor
    {
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            var all = AssetDatabase.FindAssets("t:SMGraph");
            foreach (var a in all)
            {
                SMGraph g = AssetDatabase.LoadAssetAtPath<SMGraph>(AssetDatabase.GUIDToAssetPath(a));
                if (g != null && g.changed)
                {
                    //Debug.Log($"running ContentAssetPostProcessor on {g.name}");
                    g.ResetOptions();
                }
            }
        }
    }
}