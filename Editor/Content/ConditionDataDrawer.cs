﻿using UnityEditor;

namespace com.fdt.statemachinegraph.editor
{
    [CustomPropertyDrawer(typeof(ConditionData), true)]
    public class ConditionDataDrawer : ContentDataDrawer<ConditionsAsset>
    {
        protected override string contentPropName
        {
            get { return "condition"; }
        }
    }
}