﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XNode;

namespace com.fdt.statemachinegraph.editor
{
    public abstract class ContentAssetDrawer<T> : PropertyDrawer where T:SMContentAsset
    {
        protected abstract List<T> objectsList(SMGraph graph);
        protected abstract string[] objectsOptions(SMGraph graph);
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            var graph = (((Node) property.serializedObject.targetObject).graph as SMGraph);
            if (objectsOptions(graph).Length == 0)
            {
                graph.ResetOptions();
            }
            int idx = GetCurrent(property.objectReferenceValue, graph);
            EditorGUI.BeginChangeCheck();
            idx = EditorGUI.Popup( position, label.text, idx, objectsOptions(graph));
            if (EditorGUI.EndChangeCheck())
            {
                T o = GetObjectFromIdx(idx, graph);
                property.objectReferenceValue = o;
                property.serializedObject.ApplyModifiedProperties();
                property.serializedObject.Update();
            }
            EditorGUI.EndProperty();
        }
        private T GetObjectFromIdx(int idx, SMGraph graph)
        {
            if (idx == 0)
                return null;
            return objectsList(graph)[idx-1 ];
        }

        private int GetCurrent(Object propertyObjectReferenceValue, SMGraph graph)
        {
            if (propertyObjectReferenceValue == null)
                return 0;
            var objList = objectsList(graph);
            for (int i = 0; i < objList.Count; i++)
            {
                if (objList[i] == propertyObjectReferenceValue)
                    return i + 1;
            }
            return 0;
        }
    }
}