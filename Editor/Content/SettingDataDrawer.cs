﻿using System;
using UnityEditor;
using UnityEngine;

namespace com.fdt.statemachinegraph.editor
{
    [CustomPropertyDrawer(typeof(SettingData), true)]
    public class SettingDataDrawer : ContentDataDrawer<StateSettingAsset>
    {
        private Type GetTypeOfProperty(SerializedProperty v1)
        {
            var o = SerializedPropertyExtensions.GetValue<UnityEngine.Object>(v1);
            
            Debug.Log(string.Format("{0} -- {1} -- {2}", v1.type, v1.propertyType, o));
            return default(System.Type);
        }
        protected override string contentPropName
        {
            get { return "setting"; }
        }
    }
}
