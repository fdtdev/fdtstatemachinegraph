﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace com.fdt.statemachinegraph.editor
{
    public class SMNodeBaseNodeEditor : NodeEditor
    {
        protected static GUIStyle _currentBodyStyle;

        protected virtual float LabelWidth
        {
            get { return 70; }
        }
        
        public override void OnBodyGUI()
        {
            serializedObject.Update();

            float oldLabelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = LabelWidth;

            NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("enter"), true);
            
            DrawNodeContent();

            serializedObject.ApplyModifiedProperties();
            
            EditorGUIUtility.labelWidth = oldLabelWidth;
        }
        
        protected virtual void DrawNodeContent()
        {

        }

        public static GUIStyle currentBodyStyle
        {
            get
            {
                if (_currentBodyStyle == null)
                {
                    CreateCurrentBodyStyle();
                }
                return _currentBodyStyle;
            }
        }
        private static Texture2D _currentNodeBody;
        public static Texture2D currentNodeBody { get { return _currentNodeBody != null ? _currentNodeBody : _currentNodeBody = Resources.Load<Texture2D>("current_node"); } }
        protected static void CreateCurrentBodyStyle()
        {
            _currentBodyStyle = new GUIStyle();
            _currentBodyStyle.normal.background = currentNodeBody;
            _currentBodyStyle.border = new RectOffset(32, 32, 32, 32);
            _currentBodyStyle.padding = new RectOffset(16, 16, 4, 16);
        }

        public override GUIStyle GetBodyStyle()
        {
            if (Application.isPlaying)
            {
                bool selected = SMGraphEditor.smPlayer != null && SMGraphEditor.smPlayer != null &&
                                SMGraphEditor.smPlayer.currentNode == target;
                if (selected)
                {
                    return currentBodyStyle;
                }
            }
            return base.GetBodyStyle();
        }

        public override Color GetTint()
        {
            if (Application.isPlaying)
            {
                bool selected = SMGraphEditor.smPlayer != null && SMGraphEditor.smPlayer != null &&
                                SMGraphEditor.smPlayer.currentNode == target;
                if (selected)
                {
                    SMNodeBase cTarget = target as SMNodeBase;
                    //return base.GetTint() + new Color32(51,51,51, 15);
                    return base.GetTint() * 1.85f; //* 1.35f;
                }
            }
            return base.GetTint();
        }
    }

    public class SMNodeBaseEditor : UnityEditor.Editor
    {
        public static ISMPlayer currentTarget;
        
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            var cdata = GetCurrentSMEntityDataSave();
            DrawContent(cdata);
            serializedObject.ApplyModifiedProperties();
        }

        protected virtual void DrawContent(ISMPlayer cdata)
        {
            
        }

        protected ISMPlayer GetCurrentSMEntityDataSave()
        {
            var objs = Selection.gameObjects;
            foreach (var o in objs)
            {
                var d = o.GetComponent<ISMPlayer>();
                if (d != null)
                {
                    currentTarget = d;
                    return d;
                }
            }

            if (currentTarget != null)
                return currentTarget;
            return null;
        }
    }
}