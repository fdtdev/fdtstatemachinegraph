﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace com.fdt.statemachinegraph.common.editor
{
    [CustomEditor(typeof(SMGoToNode))]
    public class SMGoToNodeEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawDefaultInspector();
            serializedObject.ApplyModifiedProperties();
        }
    }

    
    
    [CustomNodeEditor(typeof(SMGoToNode))]
    public class SMGoToNodeNodeEditor : NodeEditor
    {
        public override void OnBodyGUI()
        {
            serializedObject.Update();
            float oldWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = 60;
            NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("enter"));
            var graphProp = serializedObject.FindProperty("_graph");
            NodeEditorGUILayout.PropertyField(graphProp);
            if (graphProp.objectReferenceValue != null)
            {
                var path = AssetDatabase.GetAssetPath(graphProp.objectReferenceValue);
                Object[] data = AssetDatabase.LoadAllAssetsAtPath(path);
                List<string> options = new List<string>();
                List<SMNode> optionsRef = new List<SMNode>();
                for (int i = 0; i < data.Length; i++)
                {
                    if (data[i] is SMNode)
                    {
                        options.Add(data[i].name);
                        optionsRef.Add(data[i] as SMNode);
                    }
                }

                int idx = -1;
                var nodeProp = serializedObject.FindProperty("_node");
                if (nodeProp.objectReferenceValue != null)
                {
                   
                    idx = optionsRef.IndexOf(nodeProp.objectReferenceValue as SMNode);
                  
                }

                EditorGUI.BeginChangeCheck();
                idx = EditorGUILayout.Popup(idx, options.ToArray());
                if (EditorGUI.EndChangeCheck())
                {
                    if (idx != -1)
                    {
                        nodeProp.objectReferenceValue = optionsRef[idx];
                    }
                    else
                    {
                        nodeProp.objectReferenceValue = null;
                    }

                    serializedObject.ApplyModifiedProperties();
                }
            }

            EditorGUIUtility.labelWidth = oldWidth;
            serializedObject.ApplyModifiedProperties();
        }
    }
}
