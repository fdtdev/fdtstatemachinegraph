﻿using System.Linq;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace com.fdt.statemachinegraph.editor
{
    [NodeEditor.CustomNodeEditorAttribute(typeof(SMTimeNode))]
    public class SMTimeNodeNodeEditor :  SMNodeBaseNodeEditor
    {
        public override void OnHeaderGUI()
        {
            bool HasTime = (target as SMTimeNode).HasTime;
            
            if (HasTime && Application.isPlaying)
            {
                bool selected = SMGraphEditor.smPlayer != null && SMGraphEditor.smPlayer != null &&
                                SMGraphEditor.smPlayer.currentNode == target;
                if (selected)
                {
                    float time = 0;
                    float nTime = 0;

                    var nTimeProp = serializedObject.FindProperty("_maxTime");
                    
                    SMNodeBase cTarget = target as SMNodeBase;
                    
                    time = Time.time - SMGraphEditor.smPlayer.nodeInitTime;
                    nTime = Mathf.Min(1.0f, time / nTimeProp.floatValue);
                    
                    float w = GetWidth();
                    
                    Rect r = GUILayoutUtility.GetRect(16, 15);
                    r.y += 7;
                    r.x -= 3;
                    r.width += 6;
                    r.width *= nTime;
                    
                    Color oc = GUI.color;
                    GUI.color = new Color32(0,0,0,75);
                    GUI.DrawTexture(r, Texture2D.whiteTexture, ScaleMode.StretchToFill);
                    GUI.color = oc;
                    r = GUILayoutUtility.GetLastRect();
                    r.y += 8;
                    GUI.Label(r, target.name, NodeEditorResources.styles.nodeHeader);
                    r = GUILayoutUtility.GetRect(14, 15);
                    return;
                }
            }
            base.OnHeaderGUI();
        }
        


        protected override void DrawNodeContent()
        {
            bool HasTime = (target as SMTimeNode).HasTime;
            //float time = 0;
//            float nTime = 0;
            
            if (HasTime)
            {
                var nTimeProp = serializedObject.FindProperty("_maxTime");
                EditorGUILayout.PropertyField(nTimeProp);
            }
            base.DrawNodeContent();
            if (HasTime)
            {
                NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("exit"),
                    new GUIContent("Exit by time", "Exit used when normalized time is greater than 1."), true);
            }
        }
        
    }
    
    [CustomEditor(typeof(SMTimeNode))]
    public class SMTimeNodeEditor : SMNodeBaseEditor
    {
        protected SerializedProperty nTimeProp;

        protected override void DrawContent(ISMPlayer cdata)
        {
            bool HasTime = (target as SMTimeNode).HasTime;
            base.DrawContent(cdata);
            if (HasTime)
            {
                nTimeProp = serializedObject.FindProperty("_maxTime");
                EditorGUILayout.PropertyField(nTimeProp);
                
                float time = 0;
                float nTime = 0;

                if (cdata != null && Application.isPlaying)
                {
                    time = GetTime(cdata);
                    if (time != -1)
                    {
                        nTime = time / nTimeProp.floatValue;
                        GUILayout.Label(string.Format("Current Time: {0} - normal time: {1}", time, nTime));
                        EditorGUILayout.Slider(Mathf.Clamp(nTime, 0.0f, 1.0f), 0, 1f);
                    }
                }
            }
        }
        protected float GetTime(ISMPlayer cdata)
        {
            if (cdata.currentNode == target)
            {
                return Time.time - cdata.nodeInitTime;
            }
            else
            {
                return -1;
            }
        }
    }
}
