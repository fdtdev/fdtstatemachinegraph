﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using XNodeEditor;

namespace com.fdt.statemachinegraph.editor
{
    [NodeEditor.CustomNodeEditorAttribute(typeof(SMConditionsNode))]
    public class SMConditionsNodeEditor : NodeEditor
    {
        protected SerializedProperty conditionsProp;
        protected ReorderableList conditionsList;
        
        public override void OnBodyGUI()
        {
            serializedObject.Update();
            NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("enter"), true);
            RenderConditionsList();
            NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("output"));
            serializedObject.ApplyModifiedProperties();
        }
        protected void RenderConditionsList()
        {
            if (conditionsList == null)
            {
                conditionsProp = serializedObject.FindProperty("conditions");
                conditionsList = new ReorderableList(serializedObject, conditionsProp, true, true, true, true);
                conditionsList.drawElementCallback += (rect, index, active, focused) =>
                {
                    var p =conditionsProp.GetArrayElementAtIndex(index);
                    var o = p.FindPropertyRelative("condition").objectReferenceValue;
                    Color oc = GUI.backgroundColor;
                    if (o != null)
                    {
                        GUI.backgroundColor = (o as SMContentAsset).BackColor;
                    }
                    EditorGUI.PropertyField(rect, p.FindPropertyRelative("description"), GUIContent.none, true);
                    GUI.backgroundColor = oc;
                };
                conditionsList.elementHeight = 20;
                conditionsList.elementHeightCallback += index =>
                {
                    return 20;
                };
                conditionsList.drawHeaderCallback += rect =>
                {
                    EditorGUI.LabelField(rect, "Conditions");
                };
            }
            conditionsList.DoLayoutList();
        }
    }
}