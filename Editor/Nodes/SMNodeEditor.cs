﻿using com.FDT.Common.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using XNode;
using XNodeEditor;

namespace com.fdt.statemachinegraph.editor
{
    [NodeEditor.CustomNodeEditorAttribute(typeof(SMNode))]
    public class SMNodeNodeEditor :  SMTimeNodeNodeEditor
    {
        protected override float LabelWidth
        {
            get { return 130; }
        }
        
        protected SerializedProperty settingsProp;
        protected ReorderableList settingsList;
        protected SerializedProperty conditionsProp;

        protected override void DrawNodeContent()
        {
            conditionsProp = serializedObject.FindProperty("conditions");
            
            base.DrawNodeContent();
            
            SMNode cTarget = target as SMNode;
            if (cTarget.HasSettings)
            {
                RenderSettingsList();
            }

            if (cTarget.HasConditions)
            {
                RenderConditionsList();
            }
        }
        
        protected void RenderSettingsList()
        {
            if (settingsList == null)
            {
                settingsProp = serializedObject.FindProperty("settings");
                settingsList = new ReorderableList(serializedObject, settingsProp, true, true, true, true);
                settingsList.drawElementCallback += (rect, index, active, focused) =>
                {
                    var p = settingsProp.GetArrayElementAtIndex(index);
                    bool enabledProp = p.FindPropertyRelative("enabled").boolValue;
                    var o = p.FindPropertyRelative("setting").objectReferenceValue;
                    Color oc = GUI.backgroundColor;
                    if (o != null && enabledProp)
                    {
                        GUI.backgroundColor = (o as SMContentAsset).BackColor;
                    }
                    else if (!enabledProp)
                    {
                        GUI.backgroundColor = DisabledBackgroundColor;
                    }

                    SerializedProperty descProp = p.FindPropertyRelative("description");
                    string desc = string.Empty;
                    if (string.IsNullOrEmpty(descProp.stringValue))
                    {
                        desc = $"Setting {index} ({(o==null?"null":o.name)})";
                    }
                    else
                    {
                        desc = descProp.stringValue;
                    }
                    var backColor = new Color(GUI.backgroundColor.r, GUI.backgroundColor.g, GUI.backgroundColor.b);
                    backColor.a = 0.3f;
                    EditorGUI.DrawRect(new Rect(rect.x, rect.y+2, rect.width, rect.height-6), backColor);
                    
                    EditorGUI.LabelField(rect, desc);
                    GUI.backgroundColor = oc;
                };
                settingsList.elementHeight = 20;
                settingsList.elementHeightCallback += index =>
                {
                    return 20;
                };
                settingsList.drawHeaderCallback += rect =>
                {
                    EditorGUI.LabelField(rect, "Settings");
                };
            }
            settingsList.DoLayoutList();
        }

        public static Color DisabledBackgroundColor = new Color(0.15f,0.15f,0.15f);

        protected void RenderConditionsList()
        {
            NodeEditorGUILayout.DynamicPortList("conditions", typeof(ConditionData), serializedObject, NodePort.IO.Output, Node.ConnectionType.Override, Node.TypeConstraint.None, OnCreateConditionsList);
        }

        protected void OnCreateConditionsList(ReorderableList list)
        {
            list.drawElementCallback = (rect, index, active, focused) =>
            {
                NodePort oPort = target.GetOutputPort(string.Format("conditions {0}", index));
                var p = conditionsProp.GetArrayElementAtIndex(index);
                bool enabledProp = p.FindPropertyRelative("enabled").boolValue;
                var o = p.FindPropertyRelative("condition").objectReferenceValue;
                Color oc = GUI.backgroundColor;
                if (o != null && enabledProp)
                {
                    GUI.backgroundColor = (o as SMContentAsset).BackColor;
                }
                else if (!enabledProp)
                {
                    GUI.backgroundColor = DisabledBackgroundColor;
                }
                
                SerializedProperty descProp = p.FindPropertyRelative("description");
                string desc = string.Empty;
                if (string.IsNullOrEmpty(descProp.stringValue))
                {
                    desc = $"Condition {index} ({(o==null?"null":o.name)})";
                }
                else
                {
                    desc = descProp.stringValue;
                }
                var backColor = new Color(GUI.backgroundColor.r, GUI.backgroundColor.g, GUI.backgroundColor.b);
                backColor.a = 0.3f;
                EditorGUI.DrawRect(new Rect(rect.x, rect.y+2, rect.width, rect.height-6), backColor);
                EditorGUI.LabelField(rect, desc);
                GUI.backgroundColor = oc;
                
                NodeEditorGUILayout.PortField(new Vector2(rect.x + rect.width, rect.y), oPort);
            };
            list.elementHeightCallback += index =>
            {
                return 20;
            };
            list.elementHeight = 20;
        }
    }

    
    
    [CustomEditor(typeof(SMNode), true)]
    public class SMNodeEditor : SMTimeNodeEditor
    {
        protected ReorderableList settingsList;
        
        protected ReorderableList conditionsList;

        protected SerializedProperty settingsProp;
        
        protected SerializedProperty conditionsProp;


        protected override void DrawContent(ISMPlayer cdata)
        {
            SMNode cTarget = target as SMNode;

            base.DrawContent(cdata);
            
            if (cTarget.HasSettings)
            {
                settingsProp = serializedObject.FindProperty("settings");

                if (settingsList == null)
                {
                    settingsList = new ReorderableList(serializedObject, settingsProp, false, true, false, false);
                    settingsList.drawElementCallback += (rect, index, active, focused) =>
                    {
                        float oldLabelWidth = EditorGUIUtility.labelWidth;
                        EditorGUIUtility.labelWidth = 80;
                        var p = settingsProp.GetArrayElementAtIndex(index);
                        if (!p.isExpanded)
                        {
                            p.isExpanded = true;
                            return;
                        }

                        EditorGUI.PropertyField(rect, p, true);
                        EditorGUIUtility.labelWidth = oldLabelWidth;
                    };
                    settingsList.elementHeightCallback += index =>
                    {
                        var p = settingsProp.GetArrayElementAtIndex(index);
                        if (!p.isExpanded)
                        {
                            p.isExpanded = true;
                        }

                        return p.GetHeight() + 5;
                    };
                    settingsList.drawHeaderCallback += rect => { EditorGUI.LabelField(rect, "Settings"); };
                }
                settingsList.DoLayoutList();
            }

            if (cTarget.HasConditions)
            {
                conditionsProp = serializedObject.FindProperty("conditions");
                if (conditionsList == null)
                {
                    conditionsList = new ReorderableList(serializedObject, conditionsProp, false, true, false, false);
                    conditionsList.drawElementCallback += (rect, index, active, focused) =>
                    {
                        bool isConditionsNode = false;
                        NodePort oPort = (target as SMNodeBase).GetOutputPort(string.Format("conditions {0}", index));
                        var connections = oPort.GetConnections();
                        if (connections.Count > 0 && connections[0].node is SMConditionsNode)
                        {
                            isConditionsNode = true;
                        }

                        float oldLabelWidth = EditorGUIUtility.labelWidth;
                        EditorGUIUtility.labelWidth = 80;
                        var p = conditionsProp.GetArrayElementAtIndex(index);
                        if (!p.isExpanded)
                        {
                            p.isExpanded = true;
                        }

                        if (isConditionsNode)
                        {
                            rect.x += 15;
                            rect.width -= 15;
                            GUI.Box(rect, GUIContent.none);
                            rect.x += 2;
                            rect.y += 2;
                            rect.width -= 4;
                            rect.height -= 4;
                            EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, 16),
                                p.FindPropertyRelative("description"), true);
                            /*EditorGUI.PropertyField(new Rect(rect.x, rect.y + 18, rect.width, 16),
                                p.FindPropertyRelative("timeRange"), true);*/
                            SerializedProperty trdProp = p.FindPropertyRelative("_timeRangeData");
                            float trdH = trdProp.GetHeight();
                            EditorGUI.PropertyField(new Rect(rect.x, rect.y + 18, rect.width, trdH),
                                trdProp);
                        }
                        else
                        {
                            EditorGUI.PropertyField(rect, p, true);
                        }

                        EditorGUIUtility.labelWidth = oldLabelWidth;
                    };
                    conditionsList.elementHeightCallback += index =>
                    {
                        bool isConditionsNode = false;
                        NodePort oPort = (target as SMNodeBase).GetOutputPort(string.Format("conditions {0}", index));
                        var connections = oPort.GetConnections();
                        if (connections.Count > 0 && connections[0].node is SMConditionsNode)
                        {
                            isConditionsNode = true;
                        }

                        var p = conditionsProp.GetArrayElementAtIndex(index);
                        if (!p.isExpanded)
                        {
                            p.isExpanded = true;
                        }

                        if (!isConditionsNode)
                        {
                            return EditorGUI.GetPropertyHeight(p, true);
                        }
                        else
                        {
                            return EditorGUI.GetPropertyHeight(p.FindPropertyRelative("_timeRangeData")) + 22;
                        }

                    };
                    conditionsList.drawHeaderCallback += rect => { EditorGUI.LabelField(rect, "Conditions"); };
                }
                conditionsList.DoLayoutList();
            }
        }
    }
}
